# Lenovo T520 coreboot build howto
# Tested on revision 447d9489a21aad3dda400aa519eaeaafed25573f

# get the upstream source code
git clone https://review.coreboot.org/coreboot
cd coreboot/
git submodule update --init --checkout

# fix SMBIOS type 17
git fetch origin refs/changes/08/14008/4
git show FETCH_HEAD | patch -Np1

# build crossgcc, you don't need it if you've built it
make crossgcc-x64

# then configure and build coreboot

#################### Note ####################
# first select the motherboard, and we increase the CBFS size to 3M(0x300000)
#
# then in 'General setup', you can select the `Use CMOS' option,
# so that you can use util/nvramtool to configure your machine
#
# in `Chipset', add IFD,ME,GbE
#
# in `Devices', select `Use native graphics initialization',
# don't select `Run Option ROMs...' because SeaBIOS can run VGA option ROMS,
# and we don't need other option ROMs
# And we also don't add VGA BIOS image, we'll manually add it later
#
# in `Display', you can choose to keep VESA framebuffer, but it's ok if you don't select that
#
# in `Generic Drivers', select `USB 2.0 EHCI debug dongle support' for debugging,
# and in `Console', select `USB dongle console output'
#
# And in `Payload', I use an ELF payload built by me (it's a GRUB payload with SeaBIOS)
#
make nconfig
make

# Now we've built a coreboot ROM, it can boot Linux with GRUB. We can add more components, e.g.
# nvramcui for CMOS config, coreinfo to show system info, VGA option ROM to support Windows.

# add nvramcui and coreinfo
./build/cbfstool ./build/coreboot.rom add-payload -f payloads/nvramcui/nvramcui.elf -n img/nvramcui.elf
./build/cbfstool ./build/coreboot.rom add-payload -f payloads/coreinfo/build/coreinfo.elf -n img/coreinfo.elf

# add SeaBIOS
./build/cbfstool ./build/coreboot.rom add-payload -f payloads/external/SeaBIOS/seabios/out/bios.bin.elf -n seabios.elf

# We can add the Intel VGA option ROMs, assume we already have vgabios.rom
./build/cbfstool build/coreboot.rom add -f vgabios.rom -n pci8086,0106.rom -t optionrom
# We use links file to add file aliases, we can also add the same
# option ROM several times with different names in CBFS
./build/cbfstool build/coreboot.rom add -f links -n links -t raw
