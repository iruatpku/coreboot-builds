## Introduction

This page gives you a guide for flashing the coreboot firmware in your
machines.

### Use the ROM file

If the flash file is compressed with xz, you'll need to uncompress
it with ``unxz``.

Uploaded together is a file signature. Files are signed with PGP key
0xC39274B3C6FF23D4.  You can use ``gpg --verify <file.sig>`` to verify
them.

In my new uploads, I uploaded the files with a signed SHA256 file
named <coreboot-revision>.sha256sum, you can use ``sha256sum -c
<coreboot-revision>.sha256sum`` to check the SHA256 checksums, and use
``gpg --verify <coreboot-revision>.sha256sum`` to verify it.

## Lenovo X201/xx20

The flash chips on ThinkPad X220/T420/T420s/T520 are 8MB with first
5MB IFD+ME+GbE and last 3MB BIOS. I give the last 3M on the download
page, and you just need to concat your first 3MB of the flash with it.

### Backup your factory firmware

First, you need to backup your factory firmware with a hardware flash
programmer. You should read it twice and check if they're the same.

### Flash the entire flash chip

Extract the first 5MB of the firmware which contains IFD, ME, and Gbe
firmware.

```
dd if=factory.rom of=ifd_me_gbe.bin bs=1M count=5
```

Concat the ROM for your machine.
```
cat ifd_me_gbe.bin coreboot_last3M.rom > coreboot.rom
```

Unlock the flash descriptor.
```
ifdtool -u coreboot.rom
```

Now you can flash it to your machine using a hardware flash
programmer.

### Flash only the BIOS region with flashrom

You can use the flash layout so that you can flash only the BIOS region.

First, make a 8M file with the last 3M BIOS region.

```
truncate --size 5M dummy
cat dummy coreboot-last3M.rom > coreboot.rom
```

Then flash with the layout file.

```
flashrom -p <programmer> --layout qm67-layout.txt --image bios -w coreboot.rom
```

It can also be flashed on a running Linux system. From Linux 4.5, there may be some trouble flashing with flashrom, so you need an older kernel.

```
flashrom -p internal:laptop=force_I_want_a_brick --layout qm67-layout.txt --image bios -w coreboot.rom
```

## Lenovo xx30

X230 has two flash chips with 8MB mapped at lower address and 4MB mapped at higher address in the address space. You need to flash the 4MB chip. You can just flash the 4MB ROM file directly.

If you need to flash internally after you have coreboot flashed, you also need to unlock the IFD. You can read the 8MB flash and do the IFD unlock, the flash back. I don't have a xx30 machine so I haven't tested it yet.
