#!/bin/sh

cd memdisk
grub-mkstandalone \
    -O i386-coreboot \
    --modules="ahci part_msdos part_gpt fat ext2 btrfs regexp ehci uhci ohci usb_keyboard usb at_keyboard normal terminal gfxmenu gfxterm_menu gfxterm_background png cbfs" \
    --fonts=unicode --themes=themes/gnu --locales= -o ../payload.elf $(find -type f)

#/usr/local/bin/grub-mkstandalone -O i386-coreboot --fonts= --themes= --locales= -o ../grub2.elf $(find -type f)
