#!/bin/sh
set -e

rm -rf build release payloads

./build.sh --build-grub \
	--build-seabios \
	--build-nvramcui \
	--build-coreinfo

for BOARD in t420 t520 x230
do
	./build.sh --add-seabios --add-seavgabios --add-nvramcui --add-coreinfo $BOARD
	./build.sh --payload=seabios --add-nvramcui --add-coreinfo --addblobs $BOARD
done

cd release && xz *.rom
