#!/bin/bash
set -e

BOARD=""
PAYLOAD='grub'
BUILDGCC=n
BUILDPAYLOADS=n
ADDSEABIOS=n
ADDPAYLOADS=n
ADDBLOBS=n
BASEDIR="$(pwd)"
COREBOOT="$(pwd)/coreboot"
PATCHDIR="${BOARD}/patches"

fetch() {
	cd "${BASEDIR}"
	if test ! -d coreboot; then
		git clone https://review.coreboot.org/coreboot
	fi
	cd coreboot/ && git checkout master && git pull
}

buildgcc() {
	cd "${BASEDIR}"
	if test ! -d coreboot; then
		fetch
	fi
	make -C coreboot crossgcc-i386
}

config() {
	local _BOARD="$1"
	local _PAYLOAD="$2"
	cd "${BASEDIR}"
	cp "${_BOARD}/defconfig.${_PAYLOAD}" coreboot/.config
	cd coreboot && yes '' | make oldconfig
}

build_grub() {
	cd "${BASEDIR}"
	./mkgrub.sh
	mv payload.elf "${COREBOOT}"
}

build_seabios() {
	cd "${COREBOOT}"
	make -C payloads/external/SeaBIOS/ \
		CONFIG_SEABIOS_STABLE=y \
		CONFIG_SEABIOS_VGA_COREBOOT=y \
		build
	cp payloads/external/SeaBIOS/seabios/out/bios.bin.elf "${BASEDIR}/payloads"
	cp payloads/external/SeaBIOS/seabios/out/vgabios.bin "${BASEDIR}/payloads"
}

build_nvramcui() {
	cd "${COREBOOT}"
	make nvramcui
	cp payloads/nvramcui/nvramcui.elf "${BASEDIR}/payloads"
}

build_coreinfo() {
	cd "${COREBOOT}"
	make coreinfo
	cp payloads/coreinfo/build/coreinfo.elf "${BASEDIR}/payloads"
}

add_seabios() {
	./build/cbfstool ./build/coreboot.rom \
		add-payload -f "${BASEDIR}/payloads/bios.bin.elf" \
		-n seabios.elf
}

add_seavgabios() {
	./build/cbfstool ./build/coreboot.rom \
		add -f "${BASEDIR}/payloads/vgabios.bin" \
		-n vgaroms/seavgabios.bin -t raw
}

add_nvramcui() {
	cd "${COREBOOT}"
	./build/cbfstool ./build/coreboot.rom \
		add-payload -f "${BASEDIR}/payloads/nvramcui.elf" \
		-n img/nvramcui
}

add_coreinfo() {
	cd "${COREBOOT}"
	./build/cbfstool ./build/coreboot.rom \
		add-payload -f "${BASEDIR}/payloads/coreinfo.elf" \
		-n img/coreinfo
}

addblobs() {
	# a dummy function, should be sourced from ${BOARD}/buildconf.sh
	true
}

usage() {
	cat << EOF
usage: $0 [OPTIONS] <boardname>
OPTIONS:
  --buildgcc: build crossgcc
  --payload=<payload>: set payload (default grub)
  --build-grub: build GRUB2
  --build-seabios: build SeaBIOS
  --build-nvramcui: build nvramcui
  --build-coreinfo: build coreinfo
  --add-seabios: add SeaBIOS
  --add-seavgabios: add SeaVGABIOS
  --add-nvramcui: add nvramcui
  --add-coreinfo: add coreinfo
  --addblobs: add blobs to coreboot image
EOF
}

# always fetch first
fetch

while [[ "$#" != 0 ]]
do
	case "$1" in
		--buildgcc)
			BUILDGCC=y
			shift
			;;
		--payload=*)
			PAYLOAD="${1#--payload=}"
			shift
			;;
		--build-*)
			PAYLOAD_TO_BUILD="${1#--build-}"
			install -d "${BASEDIR}/payloads"
			build_${PAYLOAD_TO_BUILD}
			shift
			;;
		--add-seabios)
			ADDSEABIOS=y
			shift
			;;
		--add-seavgabios)
			ADDSEAVGABIOS=y
			shift
			;;
		--add-nvramcui)
			ADDNVRAMCUI=y
			shift
			;;
		--add-coreinfo)
			ADDCOREINFO=y
			shift
			;;
		--addblobs)
			ADDBLOBS=y
			shift
			;;
		--help|-h)
			usage
			exit 0
			;;
		*)
			BOARD="$1"
			shift
			;;
	esac
done

if [[ "$BUILDGCC" == 'y' ]]; then
	buildgcc
fi

if [[ "x${BOARD}" == "x" ]]; then
	exit 0
fi

if test ! -x "${COREBOOT}"/util/crossgcc/xgcc/bin/iasl; then
	buildgcc
fi

# prepare source
source "${BASEDIR}/buildconf.common"
source "${BASEDIR}/${BOARD}/buildconf.sh"
prepare

config "${BOARD}" "${PAYLOAD}"
cd "${COREBOOT}" && make clean && make

if [[ "${ADDSEABIOS}" == y ]]; then
	add_seabios
fi

if [[ "${ADDSEAVGABIOS}" == y ]]; then
	add_seavgabios
fi

if [[ "${ADDNVRAMCUI}" == y ]]; then
	add_nvramcui
fi

if [[ "${ADDCOREINFO}" == y ]]; then
	add_coreinfo
fi

if [[ "${ADDBLOBS}" == y ]]; then
	addblobs
fi

install -d "${BASEDIR}/build" "${BASEDIR}/release"
ROM="${BOARD}-${PAYLOAD}-${REV:0:7}"
cp "${COREBOOT}/build/coreboot.rom" "${BASEDIR}/build/${ROM}.rom"
# generate a stripped ROM for release
strip-to-release "${ROM}"
echo "coreboot for board ${BOARD} with payload ${PAYLOAD} is built successfully."

