strip-to-release() {
    dd if="${BASEDIR}/build/${1}.rom" of="${BASEDIR}/release/${1}-last4M.rom" bs=1 skip=8M
}
