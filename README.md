## About coreboot builds

I release some prebuilt coreboot ROMs so that people interested in it can play with it.

For machines with libreboot support, I recommend using [libreboot](http://libreboot.org/).

## Builds I have

You can go to the [download](https://bitbucket.org/iruatpku/coreboot-builds/downloads) page to download the ROM files.

We now have:

* Lenovo T420 ([wiki](https://www.coreboot.org/Board%3alenovo/t420), [defconfig and build instruction](t420))

* Lenovo T520 ([wiki](https://www.coreboot.org/Board%3alenovo/t520), [defconfig and build instruction](t520))

* Lenovo X230 (NOT TESTED, [wiki](https://www.coreboot.org/Board%3alenovo/x230), [defconfig and build instruction](x230))

I've also uploaded my GRUB config for future use.

Happy hacking!

### Blobs

All builds contain Intel microcode blob.

In images with GRUB payload, native graphics initialization is used and no VGA BIOS is included. Well, you can manually add it. You may need VGA BIOS v2170 for SeaBIOS chainloaded from GRUB2.

In images with SeaBIOS payload, VGA BIOS v2170 is used. This VGA BIOS blob that comes from [Intel FSP](https://www-ssl.intel.com/content/www/us/en/intelligent-systems/intel-firmware-support-package/intel-fsp-overview.html) does not work, but the previous BGA BIOS v2170 is only 2 bytes away from the FSP one, so I think it should be safe to use. However, I'll look into how to configure a VGA BIOS later.

### OS status

* GNU/Linux: OK

* Windows 7: get stuck at disk.sys,classpnp.sys

* Windows 8.1: OK

~~Well, it seems there's some regression in these months. I'll now give a `stable' build which supports Windows 7 with SeaBIOS payload only.~~ Revision 77133af is tested, and you can view the [old build instruction](https://bitbucket.org/iruatpku/coreboot-builds/src/6275266/t420/build.sh).

Update: it seems that revision 77133af is also not stable, so I'm not thinking about the Win7 issue again.

Update again: I do not use Windows, so Windows compatibility is not guaranteed. If you have problems, feel free to fork this repo.

## How to use the script

I've made some scripts for automatic building. You need:

* git: to get the source code

* wget: to download some blobs

* grub with i386-coreboot support: to make the grub payload

You can run ``./build.sh -h`` to see how to use it.
The built file is at ``coreboot/build/coreboot.rom``.

## How to use the prebuilt ROMs

See [flash howto](flash-howto.md) page.
