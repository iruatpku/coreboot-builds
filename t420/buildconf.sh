strip-to-release() {
    dd if="${BASEDIR}/build/${1}.rom" of="${BASEDIR}/release/${1}-last3M.rom" bs=1 skip=5M
}
