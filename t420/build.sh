# Lenovo T420 coreboot build howto
# Thank coreboot community, @bios420 @florian from forum.51nb.com
# Tested on revision f14f640168ee0269b3c443cd2bba2fc8ee66e419

# get the upstream source code
git clone https://review.coreboot.org/coreboot
cd coreboot/
git submodule update --init --checkout
# apply the patch for Lenovo T420
git fetch origin refs/changes/65/11765/4
git show FETCH_HEAD | patch -p1
# patch for supporting both Sandy&Ivy native graphics init has been merged, change the Kconfig
sed -i 's/SANDYBRIDGE_LVDS/SANDYBRIDGE_IVYBRIDGE_LVDS/g' src/mainboard/lenovo/t420/Kconfig

# build crossgcc, you don't need it if you've built it
make crossgcc-x64

# then configure and build coreboot

#################### Note ####################
# first select the motherboard, and we increase the CBFS size to 2M(0x200000)
#
# then in 'General setup', you can select the `Use CMOS' option,
# so that you can use util/nvramtool to configure your machine
#
# in `Chipset', add IFD,ME,GbE
#
# in `Devices', select `Use native graphics initialization',
# don't select `Run Option ROMs...' because SeaBIOS can run VGA option ROMS,
# and we don't need other option ROMs
# And we also don't add VGA BIOS image, we'll manually add it later
#
# in `Display', you can choose to keep VESA framebuffer, but it's ok if you don't select that
#
# in `Generic Drivers', select `USB 2.0 EHCI debug dongle support' for debugging,
# and in `Console', select `USB dongle console output'
#
# And in `Payload', I use an ELF payload built by me (it's a GRUB payload with SeaBIOS)
#
make nconfig
make

# add nvramcui and coreinfo
./build/cbfstool ./build/coreboot.rom add-payload -f payloads/nvramcui/nvramcui.elf -n img/nvramcui.elf
./build/cbfstool ./build/coreboot.rom add-payload -f payloads/coreinfo/build/coreinfo.elf -n img/coreinfo.elf

# We can add the Intel VGA option ROMs, assume we already have vgabios.rom
./build/cbfstool build/coreboot.rom add -f vgabios.rom -n pci8086,0126.rom -t optionrom
# ./build/cbfstool build/coreboot.rom add -f vgabios.rom -n pci8086,0166.rom -t optionrom
# We use links file to add file aliases
# file: links
# pci8086,0166.rom pci8086,0126.rom
./build/cbfstool build/coreboot.rom add -f links -n links -t raw
